package gestionlistas;

/**
 *
 * @author ma210
 */

public class categoria {
    
    public int id;
    public String NombreGrupo;
    public String descripcion;
    categoria sig;

    public categoria(int id, String NombreGrupo, String descripcion) {
        this.id = id;
        this.NombreGrupo = NombreGrupo;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreGrupo() {
        return NombreGrupo;
    }

    public void setNombreGrupo(String NombreGrupo) {
        this.NombreGrupo = NombreGrupo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
