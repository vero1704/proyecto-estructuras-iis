package gestionlistas;

/**
 *
 * @author ma210
 */

public class usuario {
    
    public int cedula;
    public String nombre;
    public String direccion;
    public int telefono;
    usuario inicio, ultimo;
    usuario sig, ant;
    amigo sigAmig;
    
    TAGS sigTag;
    
    TAGS usuTag;

   

    public usuario(int cedula, String nombre, String direccion, int telefono) {
           this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

   

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
    
}
