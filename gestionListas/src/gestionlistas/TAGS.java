package gestionlistas;

/**
 *
 * @author ma210
 */

public class TAGS {
    
    public String nombre;
    public String descripcion;
    TAGS sig;
    TAGS ant;
    TAGS inicio;
    interes interesTag;

    public TAGS(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
