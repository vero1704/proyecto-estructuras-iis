/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionlistas;

import java.util.Date;

/**
 *
 * @author ma210
 */
public class metodo {

    usuario inicio, ultimo;
    categoria Inicio;
    TAGS inicioTags, ultimoTags;
    interes inicioInt;

    /**
     * Method that inserts users in an orderly way in a circular double list
     *
     * @param nombre Enter the username
     * @param cedula Enter the id
     * @param direccion Enter the direction
     * @param edad Enter the age
     * @return true or false
     */
    public boolean insertarOrdenadaUsuario(String nombre, int cedula, String direccion, int telefono) {

        usuario nuevo = new usuario(cedula, nombre, direccion, telefono);
        if (inicio == null) {
            inicio = nuevo;
            nuevo = ultimo;
            inicio.sig = nuevo;
            inicio.ant = ultimo;
            return true;
        }
        if (buscarUsuario(cedula) != null) {
            return false;
        }
        if (cedula < inicio.cedula) {
            nuevo.sig = inicio;
            inicio.ant = nuevo;
            nuevo.ant = inicio;
            inicio.sig = nuevo;
            inicio = nuevo;
            return true;
        }
        usuario aux = inicio;
        usuario temp = inicio;
        while (aux != null) {
            if (cedula < aux.cedula) {
                aux.ant.sig = nuevo;
                nuevo.ant = aux.sig;
                aux.ant = nuevo;
                nuevo.sig = aux;
                nuevo = ultimo;
                return true;
            }
            temp = aux;
            aux = aux.sig;
        }
        aux.ant = nuevo;
        ultimo.sig = nuevo;
        nuevo.ant = ultimo;
        inicio = nuevo;
        return true;
    }

    /**
     * Method that looks for a person by his id in the list
     *
     * @param cedula Enter the id
     * @return
     *
     *
     */
    /**
     * Method that inserts categories into a simple list ordered by id
     *
     * @param cedula Enter the id
     * @param nombreGrupo Enter group name
     * @param descripcion Enter the description
     * @return true or false
     */
    public boolean insertarTAGS(String nombre, String descripcion) {
        TAGS nuevoTags = new TAGS(nombre, descripcion);

        if (inicioTags == null) {
            inicioTags = ultimoTags = nuevoTags;
            inicioTags.sig = inicioTags;
            return true;
        }
        if (buscarTags(nombre) != null) {
            return false;
        }
        nuevoTags.sig = inicioTags;
        ultimoTags.sig = nuevoTags;
        inicioTags = nuevoTags;
        return true;
    }

    public boolean InsertAmigos(usuario usu, Date fechaAmigos, String categoria) {
        amigo nuevo = new amigo(fechaAmigos, categoria);
        usuario aux = buscarUsuario(usu.cedula);

        nuevo.sigU = usu;

        nuevo.sigA = usu.sigAmig;
        usu.sigAmig = nuevo;
        if (nuevo.sigA == null) {
            nuevo.sigA.ant = nuevo;
            return true;
        }
        nuevo.sigA = usu.sigAmig;
        usu.sigAmig = nuevo;
        return true;
    }

    public boolean Insertinteres(TAGS tag, int id) {
        String nombre;
        nombre = tag.getNombre();
        interes nuevo = new interes(id, nombre);
        if (inicioInt == null) {
            inicioInt = nuevo;
            return true;
        }
        if (buscarInteres(id) != null) {
            return false;
        }
        nuevo.sig = inicioInt;
        inicioInt = nuevo;
        return true;
    }

    public usuario buscarUsuario(int cedula) {
        usuario aux = inicio;
        do {
            if (inicio.cedula == cedula) {
                return inicio;
            }
            aux = aux.sig;
        } while (aux != inicio);
        return null;
    }

    public TAGS buscarTags(String nombre) {
        TAGS aux = inicioTags;
        do {

            if (inicioTags.nombre.equals(nombre)) {
                return inicioTags;
            }
            aux = aux.sig;
        } while (aux != inicioTags);
        return null;
    }

    public interes buscarInteres(int id) {
        interes aux = inicioInt;
        while (aux != null) {
            if (aux.id == id) {
                return aux;
            }
            aux = aux.sig;
        }
        return null;
    }

    public amigo buscarAmigo(int cedula2) {
        amigo aux = buscarAmigo(cedula2);

        while (aux != null) {
            if (aux.sigU.cedula == cedula2) {
                return aux;
            }
            aux = aux.sigA;
        }
        return null;
    }

    public boolean eliminarUsuario(int cedula) {
        usuario aux = buscarUsuario(cedula);
        if (aux != null) {
            if (aux == inicio) {
                if (inicio.sig == inicio) {
                    inicio = ultimo = null;
                    return true;
                }
                inicio = inicio.sig;
                ultimo.sig = inicio;
                inicio.ant = ultimo;
                return true;
            }
            if (aux == ultimo) {
                ultimo.ant.sig = inicio;
                ultimo = ultimo.ant;
                inicio.ant = ultimo;
                return true;
            }
            aux.ant.sig = aux.sig;
            aux.sig.ant = aux.ant;
            return true;
        }
        return false;

    }
// ELIMINAR AMIGO: requiere que el usuario ingrese la cedula del usuario al que pertenece la sublista y la cedula del amigo
    public boolean eliminarAmigos(int cedula, int cedula2) {
        usuario temp = buscarUsuario(cedula);
        amigo c = buscarAmigo(cedula2);
        if (temp.cedula == cedula) {
            if (temp.sigAmig.sigU.cedula == c.sigU.cedula) {
                temp.sigAmig = c.sigA;
            }

            return true;
        }

        return false;

    }

    //ELIMINAR TAGS: Text fiel para que el usuario ingrese el nombre del TAG
    public boolean eliminarTags(String nombre) {
        TAGS aux = buscarTags(nombre);
        if (aux != null) {
            if (aux == inicioTags) {
                if (inicioTags.sig == inicioTags) {
                    inicioTags = ultimoTags = null;
                    return true;
                }
                ultimoTags.sig = inicioTags.sig;
                inicioTags = inicioTags.sig;
                return true;
            }
            TAGS temp = inicioTags;
            while (temp.sig != aux) {
                temp = temp.sig;
            }
            if (aux == ultimoTags) {
                temp.sig = inicioTags;
                ultimoTags = temp;
                return true;
            }
            temp.sig = aux.sig;
            return true;
        }

        return false;

    }

    
    //ELIMINAR INTERES: pedir que ingrese el ID  del interes para que lo busque si existe y lo elimine 
    public boolean eliminarInteres(int id) {
        interes aux = buscarInteres(id);
        if (aux != null) {
            if (aux == inicioInt) {
                inicioInt = inicioInt;
                return true;
            }
            interes temp = inicioInt;
            while (temp.sig != aux) {
                temp = temp.sig;
            }
            temp.sig = aux.sig;
            return true;
        }
        return false;
    }

    public boolean modificarUsuario(String nombre, int cedula) {
        usuario aux = buscarUsuario(cedula);
        if (aux != null) {
            aux.nombre = nombre;
            aux.cedula = cedula;
            return true;
        }
        return false;

    }
    
    
    
//
    public boolean modificarAmigos(Date fechaAmigos, int cedula, int cedula2) {
        usuario temp = buscarUsuario(cedula);
        amigo c = buscarAmigo(cedula2);

        if (temp.cedula == cedula) {
            if (temp.sigAmig.sigU.cedula == c.sigU.cedula) {
                c.fechaAmigos = fechaAmigos ;
                
                return true;
            }
            return false;

            

        }

    return false;
    }

    public boolean modificarTags(String nombre, String descripcion) {
        TAGS aux = buscarTags(nombre);
        if (aux != null) {
            aux.nombre = nombre;
            aux.descripcion= descripcion;
            return true;
        }
        return false;
    }

    public boolean modificarInteres(int id, String nombre, String descripcion) {
        
        interes aux = buscarInteres(id);
        TAGS T = buscarTags(nombre);
        if (aux != null) {
            aux.id = id;
            T.descripcion= descripcion;
            T.nombre= nombre;
            return true;
        }
        return false;
    }
}

